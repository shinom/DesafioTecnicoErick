package steps.desafio;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Then;

public class SearchTravels  {
	
	
	public static void Driver() {
		
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver.exe");
	}
	
	static WebDriver driver = new ChromeDriver(); 
	static String baseUrl = "https://phptravels.net/"; 
	static JavascriptExecutor jse = (JavascriptExecutor)driver;
	
	
	public static void Flights() {
		
		WebElement search = driver.findElement(By.xpath("//a[@class='active_flights waves-effect']"));
		search.click();
	}
	
	public static void From() {
		WebElement from = driver.findElement(By.name("from"));
		from.click();
		from.sendKeys("Recife");
		WebElement rec = driver.findElement(By.xpath("//i[@class='mdi mdi-flight-takeoff']"));
		rec.click();
	}
	
	public static void Destination() {
		WebElement to = driver.findElement(By.name("to"));
		to.click();
		to.sendKeys("Guarulhos");
		WebElement rec = driver.findElement(By.xpath("//i[@class='mdi mdi-flight-takeoff']"));
		rec.click();
	}
	
	public static void Date() {
		WebElement date = driver.findElement(By.id("departure"));
		date.click();
		date.clear();
		date.sendKeys("17-03-2023");
	}
	
	public static void ClassType() {
		WebElement classtype = driver.findElement(By.id("flight_type"));
		classtype.click();
		
		
	}
	
	public static void Travellers() {
		WebElement travellers = driver.findElement(By.xpath("//a[@class='dropdown-toggle dropdown-btn travellers waves-effect']"));
		travellers.click();
		WebElement qtdAdulto = driver.findElement(By.xpath("//i[@class='la la-plus']"));
		qtdAdulto.click();
		WebElement submit = driver.findElement(By.xpath("//button[@class='more_details w-100 btn-lg effect ladda-button waves-effect']"));
		submit.click();	
	}
	
	public static void Check() {
		List<WebElement> list = driver.findElements(By.xpath("//div[@class='theme-search-results-item-mask-link']"));
		for(WebElement el : list) {
			jse.executeScript("arguments[0].click();", el);
			WebElement detalhes =  driver.findElement(By.xpath("//div[@class='theme-search-results-item-extend-inner']"));
			List<WebElement> titulos = detalhes.findElements(By.tagName("b"));
			
			for (WebElement titulo: titulos) {
				assertTrue("O local ou destino não está de acordo com o que foi filtrado anteriomente!", titulo.getText().equals("REC") || titulo.getText().equals("SAO"));
			}
		}
		
//O certo		
//		WebElement voo =  driver.findElement(By.xpath("//div[@class='theme-search-results-item-preview']"));
//		voo.click();
//		WebElement detalhes =  driver.findElement(By.xpath("//div[@class='theme-search-results-item-extend-inner']"));
//		List<WebElement> titulos = detalhes.findElements(By.tagName("b"));
//		
//		for (WebElement el: titulos) {
//			System.out.println(el.getText());
//		}
		

	}
	
	public static void main(String[] args) {
		driver.get(baseUrl);
		
		Flights();
		From();
		Destination();
		Date();
		ClassType();
		Travellers();
		Check();
	}

}
