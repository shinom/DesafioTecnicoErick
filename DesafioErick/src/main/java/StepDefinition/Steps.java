package StepDefinition;		

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;		

public class Steps {	
	
	
	private final static WebDriver driver = new ChromeDriver(); 
	private final String baseUrl = "https://phptravels.net/";
	static JavascriptExecutor jse = (JavascriptExecutor)driver;
	
	
    @Given("^usuário navega para a página Flights abrindo o Google Chrome$")				
    public void open_the_Chrome_and_launch_the_application() throws Throwable							
    {		driver.get(baseUrl);
	    	WebElement search = driver.findElement(By.xpath("//a[@class='active_flights waves-effect']"));
	    	search.click();					
    }		

    @When("^usuario adiciona corretamente <origem> AND <destino> AND <data> AND <travellers> AND <classtype> valores$")					
    public void enter_the_values() throws Throwable 							
    {		
    	WebElement from = driver.findElement(By.name("from"));
		from.click();
		from.sendKeys("Recife");
		WebElement rec = driver.findElement(By.xpath("//i[@class='mdi mdi-flight-takeoff']"));
		rec.click();
		
		WebElement to = driver.findElement(By.name("to"));
		to.click();
		to.sendKeys("Guarulhos");
		WebElement rec2 = driver.findElement(By.xpath("//i[@class='mdi mdi-flight-takeoff']"));
		rec2.click();
		
		WebElement date = driver.findElement(By.id("departure"));
		date.click();
		date.clear();
		date.sendKeys("17-03-2023");
		
		WebElement classtype = driver.findElement(By.id("flight_type"));
		classtype.click();
		
		WebElement travellers = driver.findElement(By.xpath("//a[@class='dropdown-toggle dropdown-btn travellers waves-effect']"));
		travellers.click();
		WebElement qtdAdulto = driver.findElement(By.xpath("//i[@class='la la-plus']"));
		qtdAdulto.click();
		WebElement submit = driver.findElement(By.xpath("//button[@class='more_details w-100 btn-lg effect ladda-button waves-effect']"));
		submit.click();	
    }		

    @Then("^usuario e direcionado para a pagina com lista filtrada$")					
    public void Reset_the_credential() throws Throwable 							
    {  		
    	List<WebElement> list = driver.findElements(By.xpath("//div[@class='theme-search-results-item-mask-link']"));
		for(WebElement el : list) {
			jse.executeScript("arguments[0].click();", el);
			WebElement detalhes =  driver.findElement(By.xpath("//div[@class='theme-search-results-item-extend-inner']"));
			List<WebElement> titulos = detalhes.findElements(By.tagName("b"));
			
			for (WebElement titulo: titulos) {
				assertTrue("O local ou destino não está de acordo com o que foi filtrado anteriomente!", titulo.getText().equals("R2wEC") || titulo.getText().equals("SAO"));
			}
		}
    }		

}